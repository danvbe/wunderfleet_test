Few words on how I setup the app.

Symfony is based on MVC pattern. 
So, I have setup a User entity (persisted in a MySql Database - I have used a single table), which holds all the data. You will find there an aditional `migration_versions` table... this comes from using DoctrineMigrations (which does ease a lot the process of development). 
Then we have the view part (4 twig files that renders the 4 stepts of the process).
And we have the Controller that does perform the job of getting data from the user, persisting in DB (via Entity) and redirecting to proper step page. I have used Symfony Forms to ease the way user data is binded to entity.
I have used cookies to store the userId and used a field on User Entity keep track of the current step.

I have used a simple bootstrap theme I have to make the form look more appealing :)


And now... to answer your questions:

1. Describe possible performance optimizations for your Code.

Well, being a small project, there is not really too much to say. On DB part, having a single table and performing lookups only on primary key, I can see no optimization. On actual application part, I would combine and minify all the assets, increasing the page speed load. being relatively simple pages, I do not see much more to improve there (maybe work a bit on cache too).
On code part, I am certain that simple CURL call is not really a solution (at least not on bigger projects). I would go with Guzzle (http://guzzlephp.org/) as I have experience with it and it is very good. I didn't use it for this project as we needed to make a single API call, to only on endpoint... so I preferred to go with plain CURL :)

2. Which things can be done better, than you've done it.

Well... I would start with Guzzle (as mentioned above). Than I would go with creating a service to handle API calls (not do them directly in controller). Validation comes next (you mentioned to skip it... so I used only basic validation, without Symfony contraints). Beside these, having in mind only what you have asked, I cannot think of more improvements. 