<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package AppBundle\Controller
 *
 * @Route("/register")
 */
class UserController extends Controller
{

    CONST COOKIE_ID = 'wunderfleet_user_id';
    CONST API_URL = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    /**
     * Returns the user entity based on the ID stored in session (if any) or returns new User entity
     * @param Request $request
     *
     * @return User|null|object
     */
    private function getUserFromCookie(Request $request)
    {
        if ($userId = $request->cookies->get(self::COOKIE_ID)) {
            return $this->getDoctrine()->getRepository('AppBundle:User')->find($userId);
        }

        return new User();
    }

    /**
     * Returns the response that will redirect the user to his current step
     * @param User $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function redirectToCurrentStepResponse(User $user)
    {
        return $this->redirect(
            $this->generateUrl(
                'step',
                [
                    'step' => $user->getCurrentStep()
                ]
            )
        );
    }

    /**
     * The entry point in the whole system
     * For already started processes - redirects to proper step
     *
     * @Route("/", name="start")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function startAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUserFromCookie($request);

        return $this->redirectToCurrentStepResponse($user);
    }

    /**
     * Reset the cookie and restart the whole process
     *
     * @Route("/restart", name="restart")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function restartAction(Request $request)
    {
        $response = $this->redirectToRoute('start');

        //reset the user's cookie to allow the start of a new process
        $response->headers->clearCookie(self::COOKIE_ID);

        return $response;
    }

    /**
     * @Route("/step/{step}", name="step")
     * @param Request $request
     *
     * @param int $step
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function stepAction(Request $request, $step = 1)
    {
        /** @var User $user */
        $user = $this->getUserFromCookie($request);

        //redirect to proper step - if necessary
        if($step != $user->getCurrentStep()){
            return $this->redirectToCurrentStepResponse($user);
        }

        //Final Step - we have no form here - simplest step :)
        if($step == 4){
            return $this->render('AppBundle:User:step4.html.twig', [
                'user' => $user
            ]);
        }

        //create the form
        $formType = 'AppBundle\Form\Step'.$step.'Type';
        $formTypeClass = get_class(new $formType());
        $form = $this->createForm($formTypeClass, $user)
                     ->add('Save', SubmitType::class);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
            if ($form->isValid()) {
                //we have no user yet... we are now creating it. We will need to save the ID (after flush) in the session
                $new_user = ( null == $user->getId() );

                $em = $this->getDoctrine()->getManager();

                //step 3 needs some extra work - API sending
                if ($step == 3) {
                    $em->persist($user);
                    $em->flush();

                    //we send the data to the API
                    $result = $this->processApiCall($user);
                    //successful API call
                    if (array_key_exists('paymentDataId',$result)) {
                        $user->setPaymentDataId($result['paymentDataId']);
                        $user->setCurrentStep($step + 1);
                        $em->persist($user);
                        $em->flush();

                        $this->addFlash('success', "Step {$step} data saved succesfully");

                        return $this->redirectToCurrentStepResponse($user);

                    } else {    //invalid results in API call
                        $this->addFlash('error', "Step {$step} data could not be processed. Please try again!");
                    }
                } else {
                    $user->setCurrentStep($step + 1);
                    $em->persist($user);
                    $em->flush();

                    $this->addFlash( 'success', "Step {$step} data saved succesfully" );

                    $response = $this->redirectToCurrentStepResponse( $user );

                    if ( $new_user ) {
                        //one week cookie for new users - they just started the registration process
                        $response->headers->setCookie( new Cookie( self::COOKIE_ID, $user->getId(), time() + ( 7 * 24 * 60 * 60 ) ) );
                    }

                    return $response;
                }
            } else {    //invalid form
                $this->addFlash('error', "Step {$step} data could not be processed. Please try again!");
            }
        }

        return $this->render(
            '@App/User/step'.$step.'.html.twig',
            [
                'user' => $user,
                'form' => $form->createView(),
            ]
        );
    }


    /**
     * We are sending the data and receiving the response from API endpoint
     * @param User $user
     *
     * @return mixed
     */
    private function processApiCall(User $user)
    {

        $data = json_encode(
            [
                'customerId' => $user->getId(),
                'iban' => $user->getIban(),
                'owner' => $user->getAccountOwner(),
            ]
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::API_URL);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );

        $result = curl_exec($curl);

        curl_close($curl);

        return json_decode($result, true);
    }
}