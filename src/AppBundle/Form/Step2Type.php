<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Step2Type extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'street',
            TextType::class,
            [
                'required' => true,
                'label' => 'Street Name',
            ]
        );

        $builder->add(
            'houseNumber',
            TextType::class,
            [
                'required' => true,
                'label' => 'House Number',
            ]
        );

        $builder->add(
            'zipCode',
            TextType::class,
            [
                'required' => true,
                'label' => 'Zip Code',
            ]
        );

        $builder->add(
            'city',
            TextType::class,
            [
                'required' => true,
                'label' => 'City',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_step2';
    }
}