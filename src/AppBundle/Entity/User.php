<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="app_user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="first_name", type="string", length=255, nullable=false) */
    protected $firstName;

    /** @ORM\Column(name="last_name", type="string", length=255, nullable=false) */
    protected $lastName;

    /** @ORM\Column(name="telephone", type="string", length=20, nullable=true) */
    protected $telephone;

    /** @ORM\Column(name="street", type="string", length=255, nullable=true) */
    protected $street;

    /** @ORM\Column(name="house_number", type="string", length=5, nullable=true) */
    protected $houseNumber;

    /** @ORM\Column(name="zip_code", type="string", length=6, nullable=true) */
    protected $zipCode;

    /** @ORM\Column(name="city", type="string", length=255, nullable=true) */
    protected $city;

    /** @ORM\Column(name="account_owner", type="string", length=255, nullable=true) */
    protected $accountOwner;

    /** @ORM\Column(name="iban", type="string", length=25, nullable=true) */
    protected $iban;

    /** @ORM\Column(name="payment_data_id", type="string", length=255, nullable=true) */
    protected $paymentDataId;

    /** @ORM\Column(name="current_step", type="smallint", nullable=false) */
    protected $currentStep;

    /**
     * User constructor.
     */
    public function __construct()
    {
        //we default to first step
        $this->currentStep = 1;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAccountOwner()
    {
        return $this->accountOwner;
    }

    /**
     * @param string $accountOwner
     */
    public function setAccountOwner($accountOwner)
    {
        $this->accountOwner = $accountOwner;
    }

    /**
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getPaymentDataId()
    {
        return $this->paymentDataId;
    }

    /**
     * @param string $paymentDataId
     */
    public function setPaymentDataId($paymentDataId)
    {
        $this->paymentDataId = $paymentDataId;
    }

    /**
     * @return mixed
     */
    public function getCurrentStep() {
        return $this->currentStep;
    }

    /**
     * @param mixed $currentStep
     */
    public function setCurrentStep( $currentStep ) {
        $this->currentStep = $currentStep;
    }

}

