<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20190604165115 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('
            CREATE TABLE app_user (
                id INT AUTO_INCREMENT NOT NULL, 
                first_name VARCHAR(255) NOT NULL, 
                last_name VARCHAR(255) NOT NULL, 
                telephone VARCHAR(20) DEFAULT NULL, 
                street VARCHAR(255) DEFAULT NULL, 
                house_number VARCHAR(5) DEFAULT NULL, 
                zip_code VARCHAR(6) DEFAULT NULL, 
                city VARCHAR(255) DEFAULT NULL, 
                account_owner VARCHAR(255) DEFAULT NULL, 
                iban VARCHAR(25) DEFAULT NULL, 
                payment_data_id VARCHAR(255) DEFAULT NULL, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('
            DROP TABLE app_user
        ');
    }
}
